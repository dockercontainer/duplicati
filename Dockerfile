FROM duplicati/duplicati:canary

# set version label
LABEL maintainer="jensamberg@mail.de"

# Setup environment
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Berlin

RUN dpkg --add-architecture i386 &&\
    apt-get update -y


# Install.
RUN \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y tzdata docker.io && \
  apt-get -y autoremove && \
  apt-get -y clean 

