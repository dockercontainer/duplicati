# Duplicati with Docker inside  

This container provide duplicat with docker inside. 
With this it is possible to backup data from other docker container and stop container 
on the server itself

Here is a run example
```
docker run -d \
  --name=duplicatitest \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -p 9200:8200 \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --restart unless-stopped \
  registry.gitlab.com/dockercontainer/duplicati:feature_docker_install
```
